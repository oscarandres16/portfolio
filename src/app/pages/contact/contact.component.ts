import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
  contactForm: UntypedFormGroup | undefined;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.contactForm = this.formBuilder.group({
      fullName: [null || '', Validators.required],
      email: [null || '', Validators.required],
      subject: [null || '', Validators.required],
      message: [null || '', Validators.required],
    });
    this.contactForm.controls['fullName'].touched;
  }

  sendContactForm() {
    if (this.contactForm?.valid) {
      console.log('this.contactForm', this.contactForm);
    } else {
      this.contactForm?.markAllAsTouched();
    }
  }
}
