import { Component, OnInit } from '@angular/core';
import {
  DataComplementaryEducation,
  DataEducation,
  DataExperience,
} from 'src/app/interfaces/resume.interface';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css'],
})
export class ResumeComponent implements OnInit {
  dataEducation: DataEducation[] = [];
  dataExperience: DataExperience[] = [];
  dataComplementaryEducation: DataComplementaryEducation[] = [];

  constructor() {
    this.setDataEducation();
    this.setDataExperience();
    this.setDataComplementaryEducation();
  }

  ngOnInit(): void {}

  setDataEducation() {
    this.dataEducation = [
      {
        period: 'Abr. 2016 - Actualidad',
        company: 'Universidad Industrial de Santander',
        jobTitle: 'Ingeniero de sistemas',
        description:
          'El perfil profesional del Ingeniero de Sistemas de la UIS corresponde a un profesional capaz de: Examinar, formular, analizar, tratar y resolver problemas en grupos interdisciplinarios, informándose en forma adecuada de su naturaleza, y proponiendo como solución un modelo representativo del problema planteado, que relacione todos los componentes en él involucrados, asegurando así una solución eficiente mediante la utilización de los recursos informáticos, de comunicación y de automatización disponibles.',
        link: 'https://rsi.uis.edu.co',
      },
      {
        period: 'Ene. 2019 - Enero 2021',
        company: 'CALUMET UIS (Grupo de investigación)',
        jobTitle: 'Desarrollador fullstack',
        description:
          'El perfil profesional del Ingeniero de Sistemas de la UIS corresponde a un profesional capaz de: Examinar, formular, analizar, tratar y resolver problemas en grupos interdisciplinarios, informándose en forma adecuada de su naturaleza, y proponiendo como solución un modelo representativo del problema planteado, que relacione todos los componentes en él involucrados, asegurando así una solución eficiente mediante la utilización de los recursos informáticos, de comunicación y de automatización disponibles.',
        link: 'https://rsi.uis.edu.co',
      },
    ];
  }

  setDataExperience() {
    this.dataExperience = [
      {
        period: 'Nov. 2021 - Actualidad',
        company: 'Universidad Industrial de Santander',
        jobTitle: 'Desarrollador frontend',
        description:
          'Desarrollo de módulos para el nuevo sistema de información administrativo del ente educativo.',
        link: 'https://rsi.uis.edu.co',
      },
      {
        period: 'Abr.2021 - Nov. 2021',
        company: 'Shareppy Internacional',
        jobTitle: 'Desarrollador fullstack',
        description:
          'Actualización de aplicativo móvil para principal cooperativa financiera de la región de Santander.',
        link: 'https://play.google.com/store/apps/details?id=shareppy.core.banking.viper.mobile.comultrasan',
      },
      {
        period: 'Trabajo independiente',
        company: 'Fundación crescendo',
        jobTitle: 'Desarrollador fullstack',
        description: 'Desarrollo de portal informativa y plataforma educativa.',
        link: 'https://funcrescendo.com',
      },
      {
        period: 'Trabajo independiente',
        company: 'AIC S.A.S',
        jobTitle: 'Desarrollador fullstack',
        description:
          'Diseño y desarrollo de portal informativo para importante constructora del departamento de La Guajira.',
        link: 'https://aicsas.com.co',
      },
    ];
  }

  setDataComplementaryEducation() {
    this.dataComplementaryEducation = [
      {
        platform: 'Udemy',
        courseName: 'Angular: De cero a experto (Legacy)',
        date: '18 de Marzo de 2021',
        image: '../../../assets/img/Education/Angular.png',
        numberHours: '35.5',
        teacher: 'Fernando Herrera',
      },
      {
        platform: 'Udemy',
        courseName: 'Bootstrap 4 | TODO sobre diseño web con BOOTSTRAP',
        date: '17 de Abril de 2020',
        image: '../../../assets/img/Education/Bootstrap 4.png',
        numberHours: '11.5',
        teacher: 'Carlos Valdez',
      },
      {
        platform: 'Udemy',
        courseName:
          'CSS desde cero: Desarrollo Web/HTML/CSS3 para profesionales',
        date: '16 de Agosto de 2019',
        image: '../../../assets/img/Education/Css desde cero.png',
        numberHours: '',
        teacher: 'Josué E. Pizano',
      },
    ];
  }
}
