export interface DataEducation {
  period: string;
  company: string;
  jobTitle: string;
  description: string;
  link: string;
}

export interface DataExperience {
  period: string;
  company: string;
  jobTitle: string;
  description: string;
  link: string;
}

export interface DataComplementaryEducation {
  platform: string;
  courseName: string;
  date: string;
  teacher: string;
  numberHours: string;
  image: string;
}
