import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  menuData: any[] = [];

  constructor() {
    this.setMenuData();
  }

  ngOnInit(): void {}

  setMenuData() {
    this.menuData = [
      {
        title: 'Sobre mí',
        link: 'home',
      },
      {
        title: 'Resumen',
        link: 'resume',
      },
      {
        title: 'Portafolio',
        link: 'portfolio',
      },
      {
        title: 'Blog',
        link: 'blog',
      },
      {
        title: 'Contacto',
        link: 'contact',
      },
    ];
  }
}
